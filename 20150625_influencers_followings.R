library(twitteR)
setwd("/Users/ahmad-ammari/Dropbox/Work/eRevalue/Code/sentiment_analytics_r/Twitter_Users")
twitter_keys <- read.table("twitter_app_keys.csv", sep=",", header = T, stringsAsFactors = F)
influencers  <- read.csv("20150625_influencers_info_sorted_by_followings.csv", sep=",", header = T, 
                         colClasses=c(rep("character",1)))
listB <- list()
influencers_ids <- as.character(influencers$user_id[1:163])
influencers_list <- lookupUsers(influencers_ids)
num_of_inf <- length(influencers_list)
query_limits <- c(15, 30, 45, 60, 75, 90, 105, 120, 135, 150)
app <- 1
query <- 0
consumer_key <- twitter_keys$ConsumerKey[app]
consumer_secret <- twitter_keys$ConsumerSecret[app]
access_token <- twitter_keys$AccessToken[app]
access_secret <- twitter_keys$AccessTokenSecret[app]

setup_twitter_oauth(consumer_key=consumer_key, consumer_secret=consumer_secret, 
                    access_token=access_token, access_secret=access_secret)
for (i in 1:num_of_inf) {
  query <- query + 1
  lB <- influencers_list[[i]]$getFriends()
  listB <- c(listB, lB)
  if (query %in% query_limits) {
    app <- app + 1
    if (app <= 10) {
      consumer_key <- twitter_keys$ConsumerKey[app]
      consumer_secret <- twitter_keys$ConsumerSecret[app]
      access_token <- twitter_keys$AccessToken[app]
      access_secret <- twitter_keys$AccessTokenSecret[app]
      setup_twitter_oauth(consumer_key=consumer_key, consumer_secret=consumer_secret, 
                          access_token=access_token, access_secret=access_secret)
    }
  }
}

df_users = data.frame(matrix(vector(), 0, 11, dimnames=list(c(),
                      c("user_id", "screen_name", "name", "influencer", "verified", "num_of_followers",
                        "num_of_followings", "num_of_tweets", "num_of_following_influencers", 
                        "created", "location"))), 
                      stringsAsFactors=F)
df_users$user_id <- as.character(df_users$user_id)
df_users$screen_name <- as.character(df_users$screen_name)
df_users$name <- as.character(df_users$name)
df_users$influencer <- as.logical(df_users$influencer)
df_users$verified <- as.logical(df_users$verified)
df_users$num_of_followers <- as.numeric(df_users$num_of_followers)
df_users$num_of_followings <- as.numeric(df_users$num_of_followings)
df_users$num_of_tweets <- as.numeric(df_users$num_of_tweets)
df_users$num_of_following_influencers <- as.numeric(df_users$num_of_following_influencers)
df_users$created <- as.character(df_users$created)
df_users$location <- as.character(df_users$location)

num_of_ppl = length(listB)
ppl_ids = character(0)
for (i in 1:num_of_ppl) {
  ppl_ids <- c(ppl_ids, listB[[i]]$id)
}
counts_ppl_ids <- table(ppl_ids)
for (i in 1:num_of_ppl) {
  user_id <- listB[[i]]$id
  screen_name <- listB[[i]]$screenName
  name <- listB[[i]]$name
  influencer <- user_id %in% as.character(influencers$user_id)
  verified <- listB[[i]]$verified
  num_of_followers <- listB[[i]]$followersCount
  num_of_followings <- listB[[i]]$friendsCount
  num_of_tweets <- listB[[i]]$statusesCount
  if (user_id %in% names(counts_ppl_ids)) {
    num_of_following_influencers <- unname(counts_ppl_ids[user_id])
  }
  else {
    num_of_following_influencers <- 0
  }
  created_date <- listB[[i]]$created
  created <- strftime(created_date, format="%d-%m-%Y")
  location <- listB[[i]]$location 
  df_temp <- data.frame(user_id, screen_name, name, influencer, verified, num_of_followers,
                        num_of_followings, num_of_tweets, num_of_following_influencers, 
                        created, location)
  colnames(df_temp) <- c("user_id", "screen_name", "name", "influencer", "verified", "num_of_followers",
                         "num_of_followings", "num_of_tweets", "num_of_following_influencers", 
                         "created", "location")
  df_users <- rbind(df_users,df_temp)
}
df_users_no_dups <- df_users[!duplicated(df_users[,c(1)]),]
write.csv(df_users_no_dups, "20150625_accounts_followed_by_influencers_f163.csv")
